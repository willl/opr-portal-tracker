# OPR Portal Tracker
Userscript for [Ingress Operation Portal Recon](https://opr.ingress.com/recon). Based on the idea of [OPR Tools](https://gitlab.com/1110101/opr-tools/).

This script provides a way to track all the submissions (portal candidates) that you see during your OPR sessions.

By default, no data is transmitted externally and the history exists only within the scope of localStorage within your browser.

## Installation
Userscript manager such as [Tampermonkey](https://tampermonkey.net/) required.

**Download:** https://gitlab.com/moriakaice/opr-portal-tracker/raw/master/opr-portal-tracker.user.js

## Accessing the data
Data is stored within your browser's localStorage, under the `oprpt_*` keys. The values are compressed with [lz-string](https://github.com/pieroxy/lz-string/)'s method `compressToUTF16`.

There are various methods to access it:

* click the provided `Open map` button
* use one of the two buttons added next to the Settings in OPR, which will copy the data to your clipboard
* use one of the buttons that allow you to export GeoJSON or KML files to be used in external applications
